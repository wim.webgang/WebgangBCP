Broadcast Control Panel to manage radio broadcast with spoken word content in txt files and start/end tune, up/down counter etc.
Created on Linux for Webgang on FM Radio Centraal, Antwerp, Belgium
Needs Gambas3 IDE for compilation or gambas runtime to run as compiled .gambas application
Can call editor eg kate, gedit to quickly edit text from within.
Needs player for .ogg files, e.g. /usr/bin/play (package sox)
Needs music player for mp3 files (eg Amarok/gstreamer)
